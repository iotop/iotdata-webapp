import datetime
import json
import base64
import csv
import os
import urllib.parse
import getpass
import mysql.connector
from flask import Flask, render_template, jsonify
from bson import json_util
from itertools import chain
from mysql.connector import Error
from dotenv import load_dotenv

app = Flask(__name__)
env_file = os.path.join(os.path.dirname(__file__), '.env')
load_dotenv(env_file)

# ENV Variables
DB_USER = os.getenv('DB_USER')
DB_PASS = os.getenv('DB_PASS')
IP = os.getenv('DB_IP')
PORT = os.getenv('DB_PORT')
DB = os.getenv('DATABASE')
APP_HOST = os.getenv('APP_HOST')
APP_PORT = os.getenv('APP_PORT')
DEBUG = os.getenv('DEBUG')
app.debug = DEBUG

connection = mysql.connector.connect(host=IP,
                                    database=DB,
                                    user=DB_USER,
                                    password=DB_PASS)

#Decoding Functions
def base64ToString(encoded):
    # Decode Function that returns a decoded version of the input, if it requires it
    # Otherwise, returns the input without decoding it (in-case it tries to decode plaintext)
    try:
        return base64.b64decode(encoded).decode('utf-8')
    except:
        return encoded

#Webapp Routes
@app.route('/')
@app.route('/home')
@app.route('/index')
def index():
    title="Home"
    return render_template('index.html', title=title), 200

@app.route('/data', methods=['GET'])
@app.route('/data/<app_id>', methods=['GET'])
def getData(app_id=None):
    if app_id != None: # Get information for a single node
        title = "Node Data: " + app_id
        node = app_id
    else: # Get information for all nodes
        title = "All Node Data"
        node = "All Data"
    return render_template('data.html', title=title, node=node), 200

@app.route('/dataAPI', methods=['GET'])
@app.route('/dataAPI/<node>', methods=['GET'])
def getNodeData(node=None):
    result = [] # Create a blank array for data results
    # Attempt a MySQL Connection
    try:
        connection = mysql.connector.connect(host=IP,
                                            database=DB,
                                            user=DB_USER,
                                            password=DB_PASS)
        if connection.is_connected():
            db_Info = connection.get_server_info() # Get the Server Information
            print("Connected to MySQL Server version ", db_Info) # Debug info that shows where the app is connecting to

            if node != None: # Check if the user is on a page for a single node
                searchQuery = "SELECT DISTINCT dev_id, payload, time FROM data WHERE dev_id = '" + node + "' ORDER BY time DESC LIMIT 100;"
                # Set search query for the specific node, selecting the top 100 and ordering by time in a descending order
            else:
                searchQuery = "SELECT DISTINCT dev_id, payload, time FROM data ORDER BY dev_id, time DESC;"
                # Set search query for all nodes, selecting the top 100 entrys for each node and ordering by time in a descending order
            cursor = connection.cursor() # Create a cursor for the connection to the database
            cursor.execute(searchQuery) # Execute the Search Query defined above
            records = cursor.fetchall() # Fetch all records from the search query
            for device in records:
                    entry = [None] * 3 # Create a new 
                    entry[0] = device[0]
                    entry[1] = base64ToString(device[1])
                    entry[2] = str(device[2])
                    result.append(entry)
    except Error as e: # Returns an error
        return str(e), 500
    return jsonify(result), 200 # Return results from query in a JSON format

@app.route('/dataNode', methods=['GET']) 
def getNodeName(): # Get only the names for every node
    try:
        searchQuery = "SELECT DISTINCT dev_id FROM data" # Search query which looks for the device name of the node
        cursor = connection.cursor() # Create a cursor for the connection to the database
        cursor.execute(searchQuery) # Execute the Search Query defined above
        records = cursor.fetchall() # Fetch all records from the search query
    except Error as e:
        return str(e), 500
    return jsonify(records), 200

@app.errorhandler(404)
def page_not_found(e):
    title="404: Page Not Found"
    return render_template('404.html', title=title, error=e)

if __name__ == "__main__":
    app.run(host=APP_HOST, port=APP_PORT)