# Database Storage Web App

A Flask-Based Web-App which contains all information in the MongoDB Database.

The Web App is currently configured to display the latest 100 results of data from each registered node currently connected to the MongoDB Database.
The app also provides the option to display a single node's data for a user to look through.

At this current point in time, the Web App is limited to the Otago Polytechnic network, so outside users cannot access the site in it's current form.

For Otago Polytechnic Users, to access the site, click [Here](http://10.118.26.12)

# Starting the Web App

Starting the Web App is a simple process. You will need access to the Virtual Machine that it's being hosted on.
The Web App is currently running on a service where it will automatically attempt to restart itself if it were to encounter a sudden shutdown (without being shut down by a user).
I would recommend MobaXTerm for this, as it provides more options when connecting to the server.

## Logging In

The credentials for the VM are:
```
IP: 10.188.26.12
Port: 3000
Username: user
```

You will need to collect a staging .ppk file from our sensitive repository as well.

![Connecting to DB](static/img/sshConnect.png)

## Controlling the Web App

Once in the Virtual Machine, you can start the Web App by typing the following command:
```
sudo systemctl start web-app.service
```

The command itself most likely won't provide any feedback, however you can check on the service with the following command:
```
sudo systemctl status web-app.service
```

### All Commands regarding the Web App

The following commands let you start, stop and see the current status of the web app.

```
sudo systemctl stop web-app.service
sudo systemctl start web-app.service
sudo systemctl status web-app.service
```

# Running an Instance of the Web App on a Local Machine

If you need to work on this application on a local machine, there's a few steps involved in creating an environment for that.

## Anaconda Navigator

First, you'll need Anaconda Navigator. The installation instructions for it can be found Below:

Windows: [Windows Install](https://docs.anaconda.com/anaconda/install/windows/)
Macintosh: [Mac OS Install](https://docs.anaconda.com/anaconda/install/mac-os/)
Linux: [Linux Install](https://docs.anaconda.com/anaconda/install/linux/)

Once you've downloaded and installed Anaconda Navigator, Open it up and go to the Environments Tab

![Anaconda Environments](static/img/anacondaEnvironments.png)

## Creating a New Environment

Create a New Environment for the Web App by clicking the Create button at the bottom of the page.

![Anaconda Create](static/img/anacondaCreateButton.png)

Give the Environment a relevant name, and make sure the checkbox for the Package Python 3.7 is selected. Then click Create.

Once the Environment is created and done loading the required packages, select it in the Environments list, and click the Arrow, which will bring up a drop-down list.
Select "Open Terminal" From the list, You'll be greeted by a CLI, with a line showing the name of your environment, and it's current directory it's in.

![Anaconda Terminal](static/img/anacondaTerminal.png)

## Setting up the Environment

Change the directory to the required one using the `cd` Command. If required directory is on another Drive, simply type the name of the drive. For example, if the required
directory is on the H:\ drive, just type `H:`
From there, you can type `cd H:\link\to\the\directory` replacing the folder heirachy with the correct one for your application.

![Anaconda Terminal 2](static/img/anacondaTerminal2.png)

Once you're in the required directory, you won't be able to just instantly start the app like you would with the current Live Site's Virtual Machine. You'll need to download a few
requirements for your Environment before it can run the app.

The required modules, along with the code to install them for the environment are as follows:
```
pip install flask
pip install python-dotenv
pip install mysql-connector-python
```

## Setting up a .ENV File

After you've successfully installed these modules, you'll need to do one final thing before running the app. If you noticed, we installed python-dotenv, allowing us to use a .ENV
file for the server. Unfortunately, this file isn't part of the repository, as it contains sensitive information which can't be provided on this repository since it's public.
You can collect a .env file from the sensitive repository, which contains all required login information for connecting to MongoDB. This information will only work on the Polytechnic
Network, so unfortunately at this point you cannot run a local environment at home.

Once you've got yourself a .ENV file containing the required information for running the application. You can run it on your local machine by typing in the command `python app.py`
All going well, you'll be greeted by something similar to this:

![Anaconda Python App](static/img/anacondaTerminal3.png)

Don't worry too much if you haven't got the 'GET' lines, but hopefully you'll have a message about the app running on 'localhost:8080' If you do, great! If you've been met by errors,
chances are you've missed one of the modules required for the app. The errors should tell you what you're missing if it does come up with any.

Finally, to see if you can connect to the app, open a browser of your choice, and type `localhost:8080` into the URL bar. All going well, it should open up to the home page of the web app.
If it comes up with an error about the site cannot be reached, double check to see if the console has any errors, causing the app to stop.

And that's it. You should hopefully be able to run a new copy of the application on your PC without too much hassle.