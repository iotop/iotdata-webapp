//Variables
var $data;
var $load;
var $nodes;
var $page;
var $location;
var $basedir;

// Allocate JQuery variables on page load.
$(document).ready(function() {
    $data = $("#data");
    $load = $("#load");
    $nodes = $("#nodes");
    $page = $("#page");
    $location = $("#currentLocation");
    $basedir = $("#baseDir");
    $data.hide();
    // Update the breadcrumb navigation for the base url for /data
    if (location.href.split(location.host)[1] === "/data") {
        changeNavigation();
    }
});

// Parse the data from the AJAX request below to a readable format in a Table
function parseData(data) {
    $data.text("");
    // Save the html in plain-text as JQuery does not like leaving open tags
    var html = "";
    html += "<table class='table table-hover'>";
    html += "<thead><tr><th>Node Data</th><th>Log Date</th><th>Log Time</th></tr></thead><tbody>";
    // Run through each piece of data
    for(var i = 0; i <= data.length - 1; i++)
    {
        // Add the names of each node only if the user is on the allData page
        if (location.href.split(location.host)[1] === "/data") {
            // Check if the loop is on 0, to prevent an IndexOutOfBounds Error
            if (i == 0)
            {
                html += "<tr class='table-secondary'><td colspan='3'><button class='btn btn-secondary' id=" + data[0][0] + " onclick='toggleDevice(this.id)'>" + data[0][0] + "</button></td></tr>"
            }
            else
            {
                // Check if the device isn't the same as the previous entry, so that it can add the device's name to the table
                if (data[i][0] != data[i-1][0])
                {
                    html += "<tr class='table-secondary'><td colspan='3'><button class='btn btn-secondary' id=" + data[i][0] + " onclick='toggleDevice(this.id)'>" + data[i][0] + "</button></td></tr>"
                }
            }
        }
        // Split the time and date apart from each other
        
        dbTime = data[i][2].split(" ");
        html += "<tr class=" + data[i][0] + "><td>" + data[i][1] + "</td><td>" + dbTime[0] + "</td><td>" + dbTime[1] + "</td></tr>";
    }
    html += "</tbody></table>";
    // Append the html to the data div
    $data.append(html);
    $load.hide();
    $data.show();
}
// Function to Toggle the visibility of a single node's data
function toggleDevice(buttonID) {
    $("." + buttonID).toggle();
}

// Function which changes the navigation depending on what page it's on, used for the all data page
function changeNavigation() {
    $location.hide();
    $basedir.addClass("active");
    $basedir.html("Data");
}

// Add a navigation bar at the top containing all nodes 
function getNodeNames(nodes) {
    html = "<ul class='navbar-nav mr-auto'>";
    for(var i = 0; i <= nodes.length - 1; i++)
    {
        link = "../data/" + nodes[i]; // Create the Link for the device
        if (nodes[i].includes("room-sensor")) // Clean up the Navbar by changing room-sensor to RS
        {
            node = "RS" + nodes[i].substr(12)
        } else
        {
            node = nodes[i];
        }
        html += "<li class='nav-item'><a class='nav-link' href=" + link + ">" + node + "</a></li>";
    }
    html +="</ul>"
    $nodes.html(html);
}

// AJAX Requests

// Only run these requests if the user is on the /data page or any sub-URLs from the /data URL
if(location.href.split(location.host)[1].includes("/data")) {
    var CL = location.href.split(location.host)[1];
    var URL = CL.replace("data", "dataAPI");
    // Get the latest 100 entries for the specified node (specified by the URL of the page)
    $.get(URL, function(response) {
        $data = $("#data");
        $load = $("#load");
        parseData(response); // Parse the response from the AJAX request
        console.log("Data Page Loaded")
    }).fail(function(){
        console.log("An error occurred when loading the page.");
    });
    // Get the name of every node for the navigation bar at the top
    $.get("/dataNode", function(nodes) {
        getNodeNames(nodes);
        console.log("Nodes Loaded")
    }).fail(function(){
        console.log("An error occurred when loading the page.");
    });
}